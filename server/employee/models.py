from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class Employee(models.Model):
    employee_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    gender = models.CharField(max_length=1)
    age = models.IntegerField()
    contact_details = models.TextField(default="")
    email_id = models.EmailField(max_length=255)
    pass

# class User(AbstractUser):
#     admin_id = models.AutoField(primary_key=True)
#     first_name = models.CharField(max_length=80)
#     last_name = models.CharField(max_length=80)
#     gender = models.CharField(max_length=1)
#     age = models.IntegerField()
#     contact_address = models.CharField(max_length=255)
#     email = models.EmailField(max_length=255, unique=True)
#     password = models.CharField(max_length=20)
#     REQUIRED_FIELDS = []
#     pass

class JobDepartment(models.Model):
    job_id = models.AutoField(primary_key=True)
    job_dept = models.CharField(max_length = 50)
    job_name= models.CharField(max_length=30)
    description = models.TextField()
    salary_range = models.CharField(max_length=50)
    pass

class Qualification(models.Model):
    qual_id = models.AutoField(primary_key=True)
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    position = models.CharField(max_length=255)
    requirements = models.CharField(max_length=255)
    date_in = models.DateField()
    pass

class SalaryAndBonus(models.Model):
    salary_id = models.AutoField(primary_key=True)
    job_id = models.ForeignKey(JobDepartment, on_delete=models.CASCADE)
    amount = models.FloatField()
    annual_ctc = models.FloatField()
    bonus = models.FloatField()
    pass

class Leave(models.Model):
    leave_id = models.AutoField(primary_key=True)
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    date = models.DateField()
    reason = models.CharField(max_length=255)
    pass

class Payroll(models.Model):
    payroll_id = models.AutoField(primary_key=True)
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    job_id = models.ForeignKey(JobDepartment, on_delete=models.CASCADE)
    salary_id = models.ForeignKey(SalaryAndBonus, on_delete=models.CASCADE)
    leave_id = models.ForeignKey(Leave, on_delete=models.CASCADE)
    date = models.DateField()
    total_amount = models.FloatField()
    pass