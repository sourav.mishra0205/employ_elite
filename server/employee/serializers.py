from rest_framework import serializers
from .models import *

class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = "__all__"
        pass
    pass


# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ["admin_id", "age", "gender", "contact_address", "first_name", "last_name", "email", "password"]
#         extra_kwargs = {
#             'password': {'write_only': True}
#         }
#         pass
    
    # def create(self, validated_data):
    #     password = validated_data.pop('password', None)
    #     instance = self.Meta.model(**validated_data)
    #     if password:
    #         instance.set_password(password)
    #     instance.save()
    #     return instance
    # pass