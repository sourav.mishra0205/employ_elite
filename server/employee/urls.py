from django.urls import path
from .api.employee_api import EmployeeAPI

urlpatterns = [
    # path('register/', api.RegisterUserAPI.as_view(), name='signup'),
    # path('login/', api.LoginUserAPI.as_view(), name='login'),
    # path('user/', api.GetUserAPI.as_view(), name='user'),
    # path('logout/', api.LogoutUserAPI.as_view(), name='logout'),
    path('employees/', EmployeeAPI.as_view(), name='employees')
    ]