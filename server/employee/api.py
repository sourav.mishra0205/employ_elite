from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.decorators import api_view
from .models import Employee
from .serializers import EmployeeSerializer
import jwt, datetime


# class RegisterUserAPI(APIView):
#     def post(self, request):
#         serializer = UserSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data)
#     pass

class EmployeeInfo(APIView):
    def post(self, request):
        Employee.objects.create(
            first_name = request.data.get("first_name"),
            last_name = request.data.get("last_name"),
            gender = request.data.get("gender"),
            age = request.data.get("age"),
            email_id = request.data.get("email_id")
            
        )
        return Response({"status": "success"})

# class LoginUserAPI(APIView):
#     def post(self, request):
#         email = request.data["email"]
#         password = request.data["password"]

#         user = User.objects.filter(email=email).first()
        
#         if user is None:
#             raise AuthenticationFailed('User not found!')
#         if not user.check_password(password):
#             raise AuthenticationFailed('Incorrect Password!')
        
#         payload = {
#             'id': user.admin_id,
#             'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=15),
#             'iat': datetime.datetime.utcnow()

#         }

#         token = jwt.encode(payload, 'secret', algorithm='HS256')
#         response = Response()
#         response.set_cookie(key='token', value=token, httponly=True)
#         response.data = {'token': token}

#         return response
    
# class GetUserAPI(APIView):
#     def get(self, request):
        
#         token = request.COOKIES.get('token')
#         print("token: ", token)
        
#         if not token:
#             raise AuthenticationFailed('Unauthorized User!')
        
#         try:
#             payload = jwt.decode(token, 'secret', algorithms=['HS256'])
#         except jwt.ExpiredSignatureError:
#             raise AuthenticationFailed('Unauthorized User!')
#         print(payload)
#         user = User.objects.filter(admin_id=payload['id']).first()
#         serializer = UserSerializer(user)

#         return Response(serializer.data)
    
# class LogoutUserAPI(APIView):
#     def post(self, request):
#         response = Response()
#         response.delete_cookie('token')
#         response.data = {
#             'message': 'Success'
#         }
#         return response



# @api_view(['GET'])
# def get_employees(request):
#     query = Employee.objects.all()
#     serializer = EmployeeSerializer(query, many=True)
#     return Response({"status": 200, "response": serializer.data})