from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import AuthenticationFailed
from rest_framework import status
# from rest_framework.decorators import api_view
from employee.models import Employee
from employee.serializers import EmployeeSerializer


class EmployeeAPI(APIView):
    def get(self, request):
        employee_response = []
        try:
            employees = Employee.objects.all()
            for employee in employees:
                employee_response.append(EmployeeSerializer(employee).data)
            return Response({"status":"success", "response":employee_response}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                {"status": "Failed", "message":f"Error Occured {e}"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def post(self, request):
        try:
            serialized_data = EmployeeSerializer(request.data)
            serialized_data.is_valid()
            Employee.objects.create(
                first_name=serialized_data.get("first_name"),
                last_name=serialized_data.get("last_name"),
                age=serialized_data.get("age"),
                gender=serialized_data.get("gender"),
                email_id=serialized_data.get("email_id"),
                contact_details=serialized_data.get("contact_details")
            )
            return Response({"status":"success", "response":serialized_data}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response(
                {"status": "Failed", "message":f"Error Occured {e}"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

# class EmployeeDetailsAPI(APIView):s
#     def get(self, request, emp_id):
#         try:
#             employee = Employee.objects.get(employee_id=emp_id)
#             serialilzed_data = EmployeeSerializer(employee).data
#             return Response({"status":"success", "response": serialilzed_data}, status=status.HTTP_200_OK)
        
