from django.contrib import admin
from .models import Employee, Payroll, JobDepartment, Qualification, SalaryAndBonus, Leave

# Register your models here.
admin.site.register([Employee, Payroll, JobDepartment, Qualification, SalaryAndBonus, Leave])