from flask import Flask, render_template, request, Response, redirect
import requests


app = Flask(__name__)

def get_login_page():
    if request.method == "GET":
        url = 'http://127.0.0.1:8000/api/user/'
        response = requests.get(url=url)
        if response.status_code == 200:
            return redirect('/employelite/home/')
        return render_template('login.html')
    pass

app.add_url_rule('/employelite/', '/employelite/login', get_login_page)
app.add_url_rule('/employelite', '/employelite/login', get_login_page)
app.add_url_rule('/employelite/login', '/employelite/login', get_login_page)
app.add_url_rule('/employelite/login/', '/employelite/login', get_login_page)

@app.route('/employelite/home/', methods=["POST", "GET"])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        payload = {
            "email": email,
            "password": password
        }
        url = 'http://127.0.0.1:8000/api/login/'
        response = requests.post(url=url, data=payload)
        if response.status_code == 200:
            return render_template('home.html')
        else:
            return redirect('/employelite/login')
    
    # elif request.method == "GET":
    #     url = 'http://127.0.0.1:8000/api/user/'
    #     response = requests.get(url=url)
    #     if response.status_code == 200:
    #         return render_template('home.html')
    #     else:
    #         return redirect('/employelite/login')
    pass

    
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
